<?php

include 'config.php';
require ('../vendor/autoload.php');


$notification        = "";
$created_at       = "";
$sukses = (isset($_GET['']) ? $_GET[''] : '');
$error = (isset($_GET['']) ? $_GET[''] : '');

if (isset($_GET['op'])) {
    $op = $_GET['op'];
} else {
    $op = "";
}

if (isset($_POST['simpan'])) { //untuk create
    $notification        = $_POST['notification'];
    $created_at       = $_POST['created_at'];

    if ($notifiation && $created_at ) {
        if ($op == 'edit') { //untuk update
            $sql1       = "update webhooks set notification = '$notification',created_at='$created_at', where id = '$id'";
            $q1         = mysqli_query($koneksi, $sql1);
            if ($q1) {
                $sukses = "Data berhasil diupdate";
            } else {
                $error  = "Data gagal diupdate";
            }
        } else { //untuk insert
            $sql1   = "insert into webhooks(notification,created_at) values ('$notification','$created_at')";
            $q1     = mysqli_query($koneksi, $sql1);
            if ($q1) {
                $sukses     = "Berhasil memasukkan data baru";
            } else {
                $error      = "Gagal memasukkan data";
            }
        }
    } else {
        $error = "Silakan masukkan semua data";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Mobil</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <style>
        .mx-auto {
            width: 800px
        }

        .card {
            margin-top: 10px;
        }
    </style>
</head>

<body>
<div class="card-header text-white bg-secondary">
                daftar mobil yang ada di transtrack
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">notification</th>
                            <th scope="col">created_at</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql2   = "select * from webhooks order by id desc";
                        $q2     = mysqli_query($koneksi, $sql2);
                        $urut   = 1;
                        while ($r2 = mysqli_fetch_array($q2)) {
                            $id         = $r2['id'];
                            $notification        = $r2['notification'];
                            $created_at       = $r2['created_at'];

                        ?>
                            <tr>
                                <th scope="row"><?php echo $urut++ ?></th>
                                <td scope="row"><?php echo $notification ?></td>
                                <td scope="row"><?php echo $created_at ?></td>
                                <td scope="row">          
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                    
                </table>
                <a href="welcome.php"><button type="button" class="btn btn-danger">kembali halaman utama</button></a>
                <!-- <input type="submit" name="simpan" value="Kembali Halaman utama" class="btn btn-primary" href="welcome.php" /> -->
            </div>
        </div>
</body>

</html>
