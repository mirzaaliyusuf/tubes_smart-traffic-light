<?php 
include 'config1.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Mobil</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <style>
        .mx-auto {
            width: 800px
        }

        .card {
            margin-top: 10px;
        }
    </style>
</head>
<body>
<div class="card">
            <div class="card-header text-white bg-secondary">
                daftar mobil yang telah terdaftar
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">plat mobil</th>
                            <th scope="col">tipe kendaraan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql2   = "select * from daftar_mobil order by id desc";
                        $q2     = mysqli_query($koneksi, $sql2);
                        $urut   = 1;
                        while ($r2 = mysqli_fetch_array($q2)) {
                            $id         = $r2['id'];
                            $plat_mobil        = $r2['plat_mobil'];
                            $tipe_kendaraan       = $r2['tipe_kendaraan'];

                        ?>
                            <tr>
                                <th scope="row"><?php echo $urut++ ?></th>
                                <td scope="row"><?php echo $plat_mobil ?></td>
                                <td scope="row"><?php echo $tipe_kendaraan ?></td>
                                <td scope="row">
                                    <a href="registermobil.php?op=delete&id=<?php echo $id?>" onclick="return confirm('Yakin mau delete data?')"><button type="button" class="btn btn-danger">Delete</button></a>            
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                    
                </table>
                <a href="welcome.php"><button type="button" class="btn btn-danger">kembali halaman utama</button></a>
                <!-- <input type="submit" name="simpan" value="Kembali Halaman utama" class="btn btn-primary" href="welcome.php" /> -->
            </div>
        </div>
    </body>

</html>
