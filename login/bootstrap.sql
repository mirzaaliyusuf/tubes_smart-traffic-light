-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 14, 2023 at 09:47 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bootstrap`
--

-- --------------------------------------------------------

--
-- Table structure for table `daftar_mobil`
--

CREATE TABLE `daftar_mobil` (
  `id` int(11) NOT NULL,
  `plat_mobil` longtext NOT NULL,
  `tipe_kendaraan` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daftar_mobil`
--

INSERT INTO `daftar_mobil` (`id`, `plat_mobil`, `tipe_kendaraan`) VALUES
(14, 'b 3677 c', 'ambulance'),
(15, 'b 3677 c', 'ambulance'),
(17, 'b 3677 c', 'ambulance'),
(19, 'b 3677 c', 'kenegaraan'),
(20, 'b 3677 c', 'ambulance'),
(24, 'BK 67022 Cl', 'kenegaraan'),
(25, 'BL 670221 CK', 'pejabat');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(8, 'Murahsenyum', 'murahsenyum@gmail.com', '202cb962ac59075b964b07152d234b70'),
(9, 'mirza123', 'mirzaaliyusuf45@gmail.com', '202cb962ac59075b964b07152d234b70'),
(10, '110', 'ma6620996@gmail.com', '202cb962ac59075b964b07152d234b70'),
(11, 'asep', 'amirmustaqiim822@gmail.com', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Table structure for table `webhooks`
--

CREATE TABLE `webhooks` (
  `id` int(11) NOT NULL,
  `notification` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `webhooks`
--

INSERT INTO `webhooks` (`id`, `notification`, `created_at`) VALUES
(1, '', '2023-01-11 04:20:02'),
(2, '', '2023-01-11 04:21:00'),
(3, '', '2023-01-11 04:22:01'),
(4, '', '2023-01-11 04:26:14'),
(5, '', '2023-01-11 04:28:03'),
(6, '', '2023-01-11 05:44:19'),
(7, '', '2023-01-11 05:49:14'),
(8, '', '2023-01-11 09:29:46'),
(9, '', '2023-01-11 10:22:43'),
(10, '', '2023-01-11 11:01:08'),
(11, '', '2023-01-11 11:51:11'),
(12, '', '2023-01-13 20:08:25'),
(13, '', '2023-01-13 20:08:40'),
(14, '', '2023-01-14 01:33:36'),
(15, '', '2023-01-14 01:33:37'),
(16, '', '2023-01-14 01:33:51'),
(17, '', '2023-01-14 01:41:58'),
(18, '', '2023-01-14 01:43:37'),
(19, '', '2023-01-14 01:43:43'),
(20, '', '2023-01-14 05:18:54'),
(21, '', '2023-01-14 06:40:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daftar_mobil`
--
ALTER TABLE `daftar_mobil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `webhooks`
--
ALTER TABLE `webhooks`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daftar_mobil`
--
ALTER TABLE `daftar_mobil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `webhooks`
--
ALTER TABLE `webhooks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
