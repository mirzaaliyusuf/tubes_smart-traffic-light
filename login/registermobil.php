<?php

include 'config.php';
require ('../vendor/autoload.php');

$plat_mobil        = "";
$tipe_kendaraan       = "";
$sukses = (isset($_GET['']) ? $_GET[''] : '');
$error = (isset($_GET['']) ? $_GET[''] : '');

if (isset($_GET['op'])) {
    $op = $_GET['op'];
} else {
    $op = "";
}
if($op == 'delete'){
    $id         = $_GET['id'];
    $sql1       = "delete from daftar_mobil where id = '$id'";
    $q1         = mysqli_query($koneksi,$sql1);
    if($q1){
        $sukses = "Berhasil hapus data";
    }else{
        $error  = "Gagal melakukan delete data";
    }
}
if ($op == 'edit') {
    $id         = $_GET['id'];
    $sql1       = "select * from daftar_mobil where id = '$id'";
    $q1         = mysqli_query($koneksi, $sql1);
    $r1         = mysqli_fetch_array($q1);
    $plat_mobil        = $r1['plat_mobil'];
    $tipe_kendaraan       = $r1['tipe_kendaraan'];

    if ($plat_mobil == '') {
        $error = "Data tidak ditemukan";
    }
}
if (isset($_POST['simpan'])) { //untuk create
    $plat_mobil        = $_POST['plat_mobil'];
    $tipe_kendaraan       = $_POST['tipe_kendaraan'];

    if ($plat_mobil && $tipe_kendaraan ) {
        if ($op == 'edit') { //untuk update
            $sql1       = "update daftar_mobil set plat_mobil = '$plat_mobil',tipe_kendaraan='$tipe_kendaraan', where id = '$id'";
            $q1         = mysqli_query($koneksi, $sql1);
            if ($q1) {
                $sukses = "Data berhasil diupdate";
            } else {
                $error  = "Data gagal diupdate";
            }
        } else { //untuk insert
            $sql1   = "insert into daftar_mobil(plat_mobil,tipe_kendaraan) values ('$plat_mobil','$tipe_kendaraan')";
            $q1     = mysqli_query($koneksi, $sql1);
            if ($q1) {
                $sukses     = "Berhasil memasukkan data baru";
            } else {
                $error      = "Gagal memasukkan data";
            }
        }
    } else {
        $error = "Silakan masukkan semua data";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Mobil</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <style>
        .mx-auto {
            width: 800px
        }

        .card {
            margin-top: 10px;
        }
    </style>
</head>

<body>
    <div class="mx-auto">
        <!-- untuk memasukkan data -->
        <div class="card">
            <div class="card-header">
                Registrasi Mobil
            </div>
            <div class="card-body">
                <?php
                
                if ($error) {
                ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error ?>
                    </div>
                <?php
                    header("refresh:5;url=registermobil.php");//5 : detik
                }
                ?>
                <?php
                if ($sukses) {
                ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $sukses ?>
                    </div>
                <?php
                    header("refresh:5;url=registermobil.php");
                }
                ?>
                <form action="" method="POST">
                    <div class="mb-3 row">
                        <label for="nim" class="col-sm-2 col-form-label">Plat Mobil</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="plat_mobil" name="plat_mobil" value="<?php echo $plat_mobil ?>">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="tipe_kendaraan" class="col-sm-2 col-form-label">tipe kendaraan</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="tipe_kendaraan" id="tipe_kendaraan">
                                <option value="">- tipe kendaraan -</option>
                                <option value="ambulance" <?php if ($tipe_kendaraan == "ambulance") echo "selected" ?>>ambulance</option>
                                <option value="kenegaraan" <?php if ($tipe_kendaraan == "kenegaraan") echo "selected" ?>>kenegaraan</option>
                                <option value="pejabat" <?php if ($tipe_kendaraan == "pejabat") echo "selected" ?>>pejabat</option>
                                <option value="Pemadam" <?php if ($tipe_kendaraan == "Pemadam") echo "selected" ?>>pemadam</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <input type="submit" name="simpan" value="Simpan Data" class="btn btn-primary" />
                        <a href="welcome.php"><button type="button" class="btn btn-danger">kembali halaman utama</button></a>
                    </div>
                </form>
            </div>
        </div>

    </div>
</body>

</html>
