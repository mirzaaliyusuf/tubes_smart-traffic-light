String data;
int R1 = 12;
int Y1 = 13;
int G1 = 11;
int R2 = 10;
int Y2 = 9;
int G2 = 8;
int R3 = 5;
int Y3 = 7;
int G3 = 6;
int R4 = 3;
int Y4 = 4;
int G4 = 2;
bool interupt = false;
int yellowInterval = 1000;  //interval Kuning
int greenInterval = 3000;   //interval Hijau
int sequence = 1;
unsigned long lastTime;
void setup() {
  Serial.begin(9600);
  pinMode(R1, OUTPUT);
  pinMode(R2, OUTPUT);
  pinMode(R3, OUTPUT);
  pinMode(R4, OUTPUT);
  pinMode(Y1, OUTPUT);
  pinMode(Y2, OUTPUT);
  pinMode(Y3, OUTPUT);
  pinMode(Y4, OUTPUT);
  pinMode(G1, OUTPUT);
  pinMode(G2, OUTPUT);
  pinMode(G3, OUTPUT);
  pinMode(G4, OUTPUT);

  // digitalWrite(R1, LOW);
  // digitalWrite(R2, LOW);
  // digitalWrite(R3, LOW);
  // digitalWrite(R4, LOW);
  // digitalWrite(G1, LOW);
  // digitalWrite(G2, LOW);
  // digitalWrite(G3, LOW);
  // digitalWrite(G4, LOW);
  // digitalWrite(Y1, LOW);
  // digitalWrite(Y2, LOW);
  // digitalWrite(Y3, LOW);
  // digitalWrite(Y4, LOW);
  // delay(100);

  digitalWrite(R1, HIGH);
  digitalWrite(R2, HIGH);
  digitalWrite(R3, HIGH);
  digitalWrite(R4, HIGH);
}

void loop() {
  while(Serial.available()){
  	char c = Serial.read();
    interupt = true;
    switch(c){
      case '1': jalur1(); break;
      case '2': jalur2(); break;
      case '3': jalur3(); break;
      case '4': jalur4(); break;
      case '5': interupt = false; break;
    }
  }
  if(!interupt){
  	normalTrafficLight();
  }
}

void normalTrafficLight() {
  unsigned long myTime = millis();
  if (sequence == 1) {
    if (myTime - lastTime >= greenInterval + yellowInterval * 2) {
      sequence++;
      lastTime = myTime;
    } else {
      if (myTime - lastTime >= yellowInterval * 2) {
        jalur1();
      } else {
        transisi(3, 1);
      }
    }
  } else if (sequence == 2) {
    if (myTime - lastTime >= greenInterval + yellowInterval * 2) {
      sequence++;
      lastTime = myTime;
    } else {
      if (myTime - lastTime >= yellowInterval * 2) {
        jalur2();
      } else {
        transisi(1, 2);
      }
    }
  } else if (sequence == 3) {
    if (myTime - lastTime >= greenInterval + yellowInterval * 2) {
      sequence++;
      lastTime = myTime;
    } else {
      if (myTime - lastTime >= yellowInterval * 2) {
        jalur4();
      } else {
        transisi(2, 4);
      }
    }
  } else if (sequence == 4) {
    if (myTime - lastTime >= greenInterval + yellowInterval * 2) {
      sequence = 1;
      lastTime = myTime;
    } else {
      if (myTime - lastTime >= yellowInterval * 2) {
        jalur3();
      } else {
        transisi(4, 3);
      }
    }
  }
}

void transisi(int x, int y) {
  unsigned long myTimeTransition = millis();
  if (myTimeTransition - lastTime >= yellowInterval) {
    switch (y) {
      case 1: digitalWrite(Y1, HIGH); break;
      case 2: digitalWrite(Y2, HIGH); break;
      case 3: digitalWrite(Y3, HIGH); break;
      case 4: digitalWrite(Y4, HIGH); break;
    }
    switch (x) {
      case 1: digitalWrite(R1, HIGH); break;
      case 2: digitalWrite(R2, HIGH); break;
      case 3: digitalWrite(R3, HIGH); break;
      case 4: digitalWrite(R4, HIGH); break;
    }
    switch (x) {
      case 1: digitalWrite(G1, LOW); break;
      case 2: digitalWrite(G2, LOW); break;
      case 3: digitalWrite(G3, LOW); break;
      case 4: digitalWrite(G4, LOW); break;
    }
  } else {
    switch (x) {
      case 1: digitalWrite(Y1, HIGH); break;
      case 2: digitalWrite(Y2, HIGH); break;
      case 3: digitalWrite(Y3, HIGH); break;
      case 4: digitalWrite(Y4, HIGH); break;
    }
  }
}

void jalur1() {
  digitalWrite(R1, LOW);
  digitalWrite(Y1, LOW);
  digitalWrite(G1, HIGH);

  digitalWrite(R2, HIGH);
  digitalWrite(Y2, LOW);
  digitalWrite(G2, LOW);

  digitalWrite(R3, HIGH);
  digitalWrite(Y3, LOW);
  digitalWrite(G3, LOW);

  digitalWrite(R4, HIGH);
  digitalWrite(Y4, LOW);
  digitalWrite(G4, LOW);
}

void jalur2() {
  digitalWrite(R1, HIGH);
  digitalWrite(Y1, LOW);
  digitalWrite(G1, LOW);

  digitalWrite(R2, LOW);
  digitalWrite(Y2, LOW);
  digitalWrite(G2, HIGH);

  digitalWrite(R3, HIGH);
  digitalWrite(Y3, LOW);
  digitalWrite(G3, LOW);

  digitalWrite(R4, HIGH);
  digitalWrite(Y4, LOW);
  digitalWrite(G4, LOW);
}

void jalur3() {
  digitalWrite(R1, HIGH);
  digitalWrite(Y1, LOW);
  digitalWrite(G1, LOW);

  digitalWrite(R2, HIGH);
  digitalWrite(Y2, LOW);
  digitalWrite(G2, LOW);

  digitalWrite(R3, LOW);
  digitalWrite(Y3, LOW);
  digitalWrite(G3, HIGH);

  digitalWrite(R4, HIGH);
  digitalWrite(Y4, LOW);
  digitalWrite(G4, LOW);
}

void jalur4() {
  digitalWrite(R1, HIGH);
  digitalWrite(Y1, LOW);
  digitalWrite(G1, LOW);

  digitalWrite(R2, HIGH);
  digitalWrite(Y2, LOW);
  digitalWrite(G2, LOW);

  digitalWrite(R3, HIGH);
  digitalWrite(Y3, LOW);
  digitalWrite(G3, LOW);

  digitalWrite(R4, LOW);
  digitalWrite(Y4, LOW);
  digitalWrite(G4, HIGH);
}